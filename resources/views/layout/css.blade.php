<style type="text/css" >
      
     body {
       margin: 0;
       font-family: Arial, Helvetica, sans-serif;
       text-align: justify;
       position: relative;
       
     }

    .top-container {
      background-color:  #f1f1f1;
      text-align: left;
      font-family: Impact, Charcoal, sans-serif;
      color: #007bff;
    }

    .top-container h1 {
    	margin-left: 20px ;

    }

    .eljan-right, .eljan-hide-small, .eljan-wide {
       font-family:'Segoe UI',Arial,sans-serif;

    }

    .tweeter-logo {
      font-family: fontawesome;
      text-decoration: none;
      overflow: hidden;
      line-height: 1;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      font-size: 37px;
      letter-spacing: 3px;
      color: #007bff;
      display: block;
      position: relative;
    
    }

    .tweeter-logo .dotcom {
        color: #343a40;
    }

    .eljan-right {
        float: right;
        display: block;
        margin-top: -20px;
        padding-top: 10px;
        margin-left: 5px;
    }


    .eljan-wide {
        letter-spacing: 4px;
    }

    .eljan-col, .eljan-half, .eljan-third, .eljan-twothird, .eljan-threequarter, .eljan-quarter {
        float: left;
        width: 100%;

        font-family: Verdana,sans-serif;
        font-size: 15px;
        line-height: 1.5;
        }


 	  .eljan-half, .notranslate {
      	margin:4px 0 6px 0;
      }  
    
    .header {
      padding: 10px 16px;
      background-color: rgba(0, 0, 0, 0.8);
      color: white;
    }

    .content {
      padding: 16px;
      color:white;
    }

    .sticky {
      position: fixed;
      top: 0;
      width: 100%;
    }

    .sticky + .content {
      padding-top: 102px;
    }

   img.bg {
     min-height: 100%;
     min-width: 1024px;
     width: 100%;
     height: auto;
     position: fixed;
     top: 0;
     left: 0;
     z-index: -1;
   }

   @media screen and (max-width: 1024px){
     img.bg {
     left: 50%;
     margin-left: -512px;
     z-index: -1; }
   }

	video {
	  pointer-events: none;
	}

	video::-webkit-media-controls {
	  display: none;
	}

	video::-webkit-media-controls-play-button {}

	video::-webkit-media-controls-volume-slider {}

	video::-webkit-media-controls-mute-button {}

	video::-webkit-media-controls-timeline {}

	video::-webkit-media-controls-current-time-display {}

	.because {
		 position:relative;
		 overflow: hidden;
		 width: 100%;
		 height: 550px;
	}

	#videoPlayer video {
    position:relative;
    z-index:0;
 	}

	.overlay {
    position:absolute;
    top:0;
    left:0;
    margin:0;
    z-index:1;
	}

	.overlay h2 {
		color: white;
		font-family: Impact, Charcoal, sans-serif;
		margin-top: 10px;
		padding: 10px; 
		font-size:30px;
		position: relative;
		text-transform: uppercase;
		font-size: 2em;
		letter-spacing: 4px;
		overflow: hidden;
		background: linear-gradient(90deg, #000, #fff, #000);
		background-repeat: no-repeat;
		background-size: 80%;
		animation: animate 10s linear infinite;
		-webkit-background-clip: text;
		-webkit-text-fill-color: rgba(255, 255, 255, 0);
	}

	.overlay p {

		color: white;
		font-family: Impact, Charcoal, sans-serif;
		font-size:25px;
		margin:-5px 0 0 10px;
		text-align: left;
		z-index: 1;
	}

	.overlay p::first-letter {
		font-size: 140%;
		color: #007bff;
	}

	@keyframes animate {
	  0% {
	    background-position: -500%;
	  }
	  100% {
	    background-position: 500%;
	  }
	}

	@-webkit-keyframes spinner {
	  from {
	    -webkit-transform: rotateY(0deg);
	  }
	  to {
	    -webkit-transform: rotateY(-360deg);
	  }
	}

  p.ex2:hover, p.ex2:active {
    font-size: 200%;
  }

form {
  overflow: hidden;
  margin-top:-40px;
}

input[type=text] {
  float: right;
  clear: both;
  width: 130px;
  box-sizing: border-box;
  border: 2px solid #ccc;
  border-radius: 10px;
  font-size: 12px;
  background-color: white;
  background-image: url('searchicon.png');
  background-position: 10px 10px; 
  background-repeat: no-repeat;
  padding: 12px 20px 12px 10px;
  -webkit-transition: width 0.4s ease-in-out;
  transition: width 0.4s ease-in-out;
}

input[type=text]:focus {
  width: 30%;
}

.logo-image {
  text-align: center;
  margin-top:-55px;
}

.project {
  display: block;
  margin-left: auto;
  margin-right: auto;
  position:relative;
  overflow: hidden;
 
}

.logo-image img {
  display: block;
  border-radius: 50%;
  margin-left: auto;
  margin-right: auto;
  -webkit-animation-name: spinner;
  -webkit-animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  -webkit-animation-duration: 1s;
  -webkit-transform-style: preserve-3d;

}

.logo-image:hover {
  -webkit-animation-play-state: paused;
}

.logo-image img {
 
  box-shadow: inset 0 0 20px rgba(0,0,0,0.2);

}


.headline  {
  color: white;
  font-family: Impact, Charcoal, sans-serif;
  font-size:25px;
  padding: 40px 40px;
  z-index: 1;
}

.headline a {
  font-family: Impact, Charcoal, sans-serif;
  font-size:25px;
  padding: 40px 40px;
  z-index: 1;
}
.tweeter1 {
  position: relative;
  display: inline-block;
}

.tweeter1 img {
  position:relative;
  width: 100%;
  z-index: -1;
}

.tweeter1 h2 {
  font-family: Impact, Charcoal, sans-serif;
  color: white;
  text-align: center;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
  left: 50%;
 
}

#animate1 {
  transition: transform 3s ease-out;
}

.zap {
  transform: scale(2.5, 0);
}

.gk-chesterton {
  color:#ffe5db;
  font-family: Impact, Charcoal, sans-serif;
  font-size:40px;
  z-index: 1;
}

.article1 {
  color: #E0FFFF;
  font-family: Impact, Charcoal, sans-serif;
  font-size:40px;
  padding: 20px 40px;
  z-index: 1;
}

#song-sticky {
  width:100%;
  height:10%;
  padding-top:20px;
  padding-right: 40%;
  padding-left: 2%;
  background:none;
  color:white;
  font-weight:bold;
  font-size:10px;
  text-align:left;
 }

#song-sticky iframe {
  position:fixed;    
  bottom:40px;          
  right: 20px;
}

.heart {
  display: flex;
  justify-content: center;
  align-items: center;
}

.human-heart {
  margin: 5em;
  animation: .8s infinite beatHeart;
}

@keyframes beatHeart {
  0% {
    transform: scale(1);
  }
  25% {
    transform: scale(1.1);
  }
  40% {
    transform: scale(1);
  }
  60% {
    transform: scale(1.1);
  }
  100% {
    transform: scale(1);
  }
}

.fader img {
  transition: 1s ease-in-out;
  position:relative;
  width: 100%;
  z-index: -1;
}
img.swap1, div.fader:hover img.swap2 {
 opacity: 1.0;
}
.fader:hover img.swap1, img.swap2 {
  opacity: 0;
}


 	</style>