<div>
	<article class="gk-chesterton">
	  <p class="text-center my-4" >“</p>
	  <p class="text-center my-0" >“ The way to love anything is to realize that it might be lost. ” </p>
	  <p class="text-center my-0"> – G.K Chesterton</p>
	</article>
	<script>
	   var controller = new ScrollMagic.Controller();
	</script>
	 <section class="demo">

	  <div class="spacer s2"></div>
	  <div id="trigger1" class="spacer s0"></div>
	  <div id="animate1" >
	    <img src="images/typewriter.jpeg">
	    <p>Zap.</p>
	  
	  </div>
	  <div class="spacer s2"></div>
	  <script>
	    var scene = new ScrollMagic.Scene({triggerElement: "#trigger1"})
	           
	            .setClassToggle("#animate1", "zap")
	            
	            .addTo(controller);
	  </script>
	</section>
</div>