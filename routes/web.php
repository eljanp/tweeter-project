<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () 
{
 return View::make('indexmarketing');
});

Auth::routes();


// Route::get('/login', 'HomeController@tweet');

// Route::post('login', 'HomeController@tweet');

Route::get('/tweet', 'HomeController@tweet')->name('blog');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/posts', 'PostsController@blog');

Route::get('/tweet', 'PostsController@blog');

Route::get('/post/{id}/edit', 'PostsController@edit')->name('edit');

Route::get('/post/{id}', 'PostsController@blog');

Route::get('/post', 'PostsController@create');

Route::post('/post/{id}', 'PostsController@update')->name('update');

Route::post('/post', 'PostsController@store');

Route::post('/comment', 'PostsController@storeComment');

Route::get('/post/{id}/delete', 'PostsController@delete');

Route::get('/users', 'HomeController@people');

Route::get('user/{id}', 'HomeController@user')->name('user.view');

Route::post('ajaxRequest', 'HomeController@ajaxRequest')->name('ajaxRequest');

Route::post('profile/{profileId}/follow', 'ProfileController@followUser')->name('user.follow');

Route::post('/{profileId}/unfollow', 'ProfileController@unFollowUser')->name('user.unfollow');